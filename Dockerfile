FROM openjdk:12
ADD target/docker-spring-boot-cristianoschaarschmidt.jar docker-spring-boot-cristianoschaarschmidt.jar
EXPOSE 8085
ENTRYPOINT ["java", "-jar", "docker-spring-boot-cristianoschaarschmidt.jar"]
